package main

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers"
)

func main() {
	controllers.Register()
}
