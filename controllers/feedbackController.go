package controllers

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers/utils"
	"bitbucket.org/GideonHoogeveen/radboudmaps/converters"
	"bitbucket.org/GideonHoogeveen/radboudmaps/database"
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	//"github.com/gorilla/mux"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
	"net/http"
)

type feedbackController struct {
}

func (*feedbackController) Index(w http.ResponseWriter, r *http.Request) {

	x, _, err := database.Query("MATCH (n:Feedback) RETURN n", map[string]interface{}{})

	if err != nil {
		panic(err)
	}

	var feedback []viewmodels.Feedback

	for i := 0; i < len(x); i++ {
		n := x[i][0].(graph.Node)

		feed, err := converters.NewFeedbackView(n.Properties)

		if err != nil {
			panic(err)
		}

		feedback = append(feedback, feed)
	}

	utils.WriteJSON(w, &viewmodels.Container{feedback})
}

func (*feedbackController) New(w http.ResponseWriter, r *http.Request) {
	var c viewmodels.Container
	utils.ReadJSON(r, &c)

	feedback, err := converters.NewFeedbackView(c.Data.(map[string]interface{}))

	if err != nil {
		utils.WriteJSON(w, &viewmodels.ErrorContainer{[]viewmodels.Error{viewmodels.Error{400, "Bad Request", err.Error()}}})
		return
	}

	x, _, err := database.Query("CREATE (n:Feedback {reason:{reason}, explanation:{explanation}, from:{from}, from_type:{from_type}, to:{to}, to_type:{to_type}, app_version:{app_version},user_uuid:{user_uuid}, created_at:timestamp()}) RETURN n", map[string]interface{}{"reason": feedback.Reason, "explanation": feedback.Explanation, "to": feedback.To, "to_type": feedback.ToType, "from": feedback.From, "from_type": feedback.FromType, "app_version": feedback.AppVersion, "user_uuid": feedback.UserUUID})

	if err != nil {
		panic(err)
	}
	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	feedback, err = converters.NewFeedbackView(x[0][0].(graph.Node).Properties)

	if err != nil {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{feedback})
}
