package controllers

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers/utils"
	"bitbucket.org/GideonHoogeveen/radboudmaps/converters"
	"bitbucket.org/GideonHoogeveen/radboudmaps/database"
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	"github.com/gorilla/mux"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
	"net/http"
)

type locationController struct {
}

func (*locationController) Index(w http.ResponseWriter, r *http.Request) {

	x, _, err := database.Query("MATCH (n:Location) RETURN n", map[string]interface{}{})

	if err != nil {
		panic(err)
	}

	var locations []viewmodels.Location

	for i := 0; i < len(x); i++ {
		n := x[i][0].(graph.Node)
		locations = append(locations, converters.NewLocationView(n.Properties))
	}

	utils.WriteJSON(w, &viewmodels.Container{locations})
}

func (*locationController) Get(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (n:Location {uuid:{uuid}} ) RETURN n", map[string]interface{}{"uuid": uuid})

	if err != nil {
		utils.WriteJSON(w, &viewmodels.ErrorContainer{[]viewmodels.Error{viewmodels.Error{500, "Internal Server Error", err.Error()}}})
		return
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{converters.NewLocationView(x[0][0].(graph.Node).Properties)})

}

func (*locationController) GetRooms(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (n:Location {uuid:{uuid}} ) -[:HAS_ROOM]-> (r) RETURN r", map[string]interface{}{"uuid": uuid})

	if err != nil {
		panic(err)
	}

	var rooms []viewmodels.Room

	for i := 0; i < len(x); i++ {
		n := x[i][0].(graph.Node)
		rooms = append(rooms, converters.NewRoomView(n.Properties))
	}
	utils.WriteJSON(w, &viewmodels.Container{rooms})

}

func (*locationController) Search() {

}
