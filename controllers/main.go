package controllers

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func Register() {

	mainController := &mainController{}
	locationController := &locationController{}
	roomController := &roomController{}
	routeController := &routeController{}
	peopleController := &peopleController{}
	feedbackController := &feedbackController{}

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", mainController.Index)
	router.HandleFunc("/locations/", locationController.Index).Methods("GET")
	router.HandleFunc("/locations/{uuid}", locationController.Get).Methods("GET")
	router.HandleFunc("/locations/{uuid}/rooms/", locationController.GetRooms).Methods("GET")
	router.HandleFunc("/rooms/", roomController.Index).Methods("GET")
	router.HandleFunc("/rooms/{uuid}/", roomController.Get).Methods("GET")
	router.HandleFunc("/rooms/{uuid}/location/", roomController.GetLocation).Methods("GET")
	router.HandleFunc("/route/{from}/{to}", routeController.Get).Methods("GET")

	router.HandleFunc("/people/", peopleController.Index).Methods("GET")
	router.HandleFunc("/people/{uuid}/", peopleController.Get).Methods("GET")
	router.HandleFunc("/people/{uuid}/room", peopleController.GetRoom).Methods("GET")

	//router.HandleFunc("/feedback", feedbackController.Index).Methods("GET")
	router.HandleFunc("/feedback", feedbackController.New).Methods("POST")

	router.Use(CORS)

	port := os.Getenv("PORT")

	if port == "" {
		log.Print("$PORT must be set, using default port of 8080")
		port = "8080"
	}

	arg := os.Args[1:]
	if len(arg) > 0 {
		port = arg[0]
	}

	log.Print("using port", port)

	log.Fatal(http.ListenAndServe(":"+port, router))
}

func CORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "http://www.radboudmaps.nl")
		next.ServeHTTP(w, r)
	})
}
