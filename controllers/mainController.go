package controllers

import (
	"net/http"
)

type mainController struct {
}

func (m *mainController) Index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("GET /locations/ \t\t\t all locations\n" +
		"GET /locations/{uuid} \t\t\t location by uuid\n" +
		"GET /locations/{uuid}/rooms \t\t get all rooms connected to location with given uuid\n\n" +
		"GET /rooms/       \t\t\t all rooms\n" +
		"GET /rooms/?filter=18 \t\t\t all rooms containing filter\n" +
		"GET /rooms/{uuid}   \t\t\t room by uuid\n" +
		"GET /rooms/{uuid}/location   \t\t get the location associated with the given room\n\n" +
		"GET /people/      \t\t\t all people\n" +
		"GET /people/?filter=Hans \t\t get all people containing filter\n" +
		"GET /people/{uuid} \t\t\t get person by uuid.\n\n" +
		"GET /people/{uuid}/room \t\t get the room where person with uuid resides in.\n\n" +
		"GET /route/{from}/{to}   \t\t calculate the route from location {from} and location {to}, both {from} and {to} are the uuid of the locations\n\n" +
		"POST /feedback    \t\t\t create new feedback resource"))
}
