package controllers

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers/utils"
	"bitbucket.org/GideonHoogeveen/radboudmaps/converters"
	"bitbucket.org/GideonHoogeveen/radboudmaps/database"
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	"github.com/gorilla/mux"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
	"net/http"
)

type peopleController struct {
}

func (*peopleController) Index(w http.ResponseWriter, r *http.Request) {

	filter := ""
	if len(r.URL.Query()["filter"]) >= 1 {
		filter = r.URL.Query()["filter"][0]
	}

	x, _, err := database.Query("MATCH (n:Person) WHERE toLower(n.name) CONTAINS toLower({filter}) RETURN n", map[string]interface{}{"filter": filter})

	if err != nil {
		panic(err)
	}

	var people []viewmodels.Person

	for i := 0; i < len(x); i++ {
		n := x[i][0].(graph.Node)
		people = append(people, converters.NewPersonView(n.Properties))
	}
	utils.WriteJSON(w, &viewmodels.Container{people})
}

func (*peopleController) Get(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (n:Person {uuid:{uuid}} ) RETURN n", map[string]interface{}{"uuid": uuid})

	if err != nil {
		panic(err)
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{converters.NewPersonView(x[0][0].(graph.Node).Properties)})
}

func (*peopleController) GetRoom(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (p:Person {uuid:{uuid}} ) -[:HAS_OFFICE]-> (r) RETURN r", map[string]interface{}{"uuid": uuid})

	if err != nil {
		panic(err)
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	//makes the assumption a person always has 1 office. May introduce bugs when someone has multiple offices
	utils.WriteJSON(w, &viewmodels.Container{converters.NewRoomView(x[0][0].(graph.Node).Properties)})
}
