package controllers

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers/utils"
	"bitbucket.org/GideonHoogeveen/radboudmaps/converters"
	"bitbucket.org/GideonHoogeveen/radboudmaps/database"
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	"github.com/gorilla/mux"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
	"net/http"
)

type roomController struct {
}

func (*roomController) Index(w http.ResponseWriter, r *http.Request) {

	filter := ""
	if len(r.URL.Query()["filter"]) >= 1 {
		filter = r.URL.Query()["filter"][0]
	}

	x, _, err := database.Query("MATCH (n:Room) WHERE toLower(n.code) CONTAINS toLower({filter}) RETURN n", map[string]interface{}{"filter": filter})

	if err != nil {
		panic(err)
	}

	var rooms []viewmodels.Room

	for i := 0; i < len(x); i++ {
		n := x[i][0].(graph.Node)
		rooms = append(rooms, converters.NewRoomView(n.Properties))
	}
	utils.WriteJSON(w, &viewmodels.Container{rooms})
}

func (*roomController) Get(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (n:Room {uuid:{uuid}} ) RETURN n", map[string]interface{}{"uuid": uuid})

	if err != nil {
		panic(err)
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{converters.NewRoomView(x[0][0].(graph.Node).Properties)})
}

func (*roomController) GetLocation(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uuid := vars["uuid"]

	x, _, err := database.Query("MATCH (n:Room {uuid:{uuid}} ) <-[:HAS_ROOM]- (l) RETURN l", map[string]interface{}{"uuid": uuid})

	if err != nil {
		panic(err)
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{nil})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{converters.NewLocationView(x[0][0].(graph.Node).Properties)})
}
