package controllers

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/controllers/utils"
	"bitbucket.org/GideonHoogeveen/radboudmaps/database"
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	"github.com/gorilla/mux"
	"net/http"
)

type routeController struct {
}

func (*routeController) Get(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	from := vars["from"]
	to := vars["to"]

	if from == to {
		utils.WriteJSON(w, &viewmodels.Container{[]string{"Destination reached."}})
		return
	}

	x, _, err := database.Query("MATCH (from:Location {uuid:{from}}),(to:Location {uuid:{to}}), p = shortestPath((from)-[:DIRECTIONS_TO*]->(to)) "+
		"RETURN extract(n in relationships(p) | n.description)", map[string]interface{}{"from": from, "to": to})

	if err != nil {
		utils.WriteJSON(w, &viewmodels.ErrorContainer{[]viewmodels.Error{viewmodels.Error{500, "Internal Server Error", err.Error()}}})
		return
	}

	if len(x) == 0 {
		utils.WriteJSON(w, &viewmodels.ErrorContainer{[]viewmodels.Error{viewmodels.Error{404, "Route not found.", "Unable to found a route given the two locations"}}})
		return
	}

	if len(x[0]) == 0 {
		utils.WriteJSON(w, &viewmodels.Container{[]string{"Destination reached."}})
		return
	}

	utils.WriteJSON(w, &viewmodels.Container{x[0][0]}) // add destination reached
}
