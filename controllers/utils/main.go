package utils

import (
	"encoding/json"
	"net/http"
)

func ReadJSON(req *http.Request, m interface{}) error {
	err := json.NewDecoder(req.Body).Decode(m)
	defer req.Body.Close()
	return err
}

func WriteJSON(w http.ResponseWriter, m interface{}) error {
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	w.Write(b)
	return nil
}
