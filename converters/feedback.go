package converters

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
	"fmt"
)

func NewFeedbackView(prop map[string]interface{}) (viewmodels.Feedback, error) {

	var empty viewmodels.Feedback

	uuid, ok := prop["uuid"]

	if !ok {
		uuid = ""
	}

	reason, ok := prop["reason"]
	if !ok {
		return empty, fmt.Errorf("Missing 'reason' property")
	}

	explain, ok := prop["explanation"]

	if !ok {
		return empty, fmt.Errorf("Missing 'explanation' property")
	}

	from, ok := prop["from"]

	if !ok {
		return empty, fmt.Errorf("Missing 'from' property")
	}

	if from == nil {
		from = ""
	}

	fromT, ok := prop["from_type"]

	if !ok {
		return empty, fmt.Errorf("Missing 'from_type' property")
	}

	if fromT == nil {
		fromT = ""
	}

	to, ok := prop["to"]

	if !ok {
		return empty, fmt.Errorf("Missing 'to' property")
	}

	if to == nil {
		to = ""
	}

	toT, ok := prop["to_type"]

	if !ok {
		return empty, fmt.Errorf("Missing 'to_type' property")
	}

	if toT == nil {
		toT = ""
	}

	appVer, ok := prop["app_version"]

	if !ok {
		return empty, fmt.Errorf("Missing 'app_version' property")
	}

	userUUID, ok := prop["user_uuid"]

	if !ok {
		return empty, fmt.Errorf("Missing 'user_uuid' property")
	}
	createdAt, ok := prop["created_at"]

	if !ok {
		createdAt = int64(0)
	}

	return viewmodels.Feedback{
		UUID:        uuid.(string),
		Reason:      reason.(string),
		Explanation: explain.(string),
		From:        from.(string),
		FromType:    fromT.(string),
		To:          to.(string),
		ToType:      toT.(string),
		AppVersion:  appVer.(string),
		UserUUID:    userUUID.(string),
		CreatedAt:   createdAt.(int64),
	}, nil
}
