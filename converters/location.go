package converters

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
)

func NewLocationView(properties map[string]interface{}) viewmodels.Location {
	return viewmodels.Location{
		UUID:     properties["uuid"].(string),
		Name:     properties["name"].(string),
		Code:     properties["code"].(string),
		Building: properties["building"].(string),
		Floor:    properties["floor"].(int64),
	}
}
