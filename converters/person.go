package converters

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
)

func NewPersonView(properties map[string]interface{}) viewmodels.Person {
	return viewmodels.Person{
		UUID: properties["uuid"].(string),
		Name: properties["name"].(string),
	}
}
