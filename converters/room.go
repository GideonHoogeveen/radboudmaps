package converters

import (
	"bitbucket.org/GideonHoogeveen/radboudmaps/viewmodels"
)

func NewRoomView(properties map[string]interface{}) viewmodels.Room {
	return viewmodels.Room{
		UUID:     properties["uuid"].(string),
		Code:     properties["code"].(string),
		Building: properties["building"].(string),
		Floor:    properties["floor"].(int64),
	}
}
