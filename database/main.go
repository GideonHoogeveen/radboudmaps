package database

import (
	"fmt"
	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
	"os"
)

var driver bolt.Driver
var dbBolt string

func init() {
	driver = bolt.NewDriver()
	dbBoltUrl, ok := os.LookupEnv("RADBOUDMAPS_DB_BOLT_URL")

	if !ok {
		panic(fmt.Errorf("RADBOUDMAPS_DB_BOLT_URL environment variable not set"))
	}

	dbBoltUser, ok := os.LookupEnv("RADBOUDMAPS_DB_BOLT_USER")

	if !ok {
		panic(fmt.Errorf("RADBOUDMAPS_DB_BOLT_USER environment variable not set"))
	}

	dbBoltPass, ok := os.LookupEnv("RADBOUDMAPS_DB_BOLT_PASSWORD")

	if !ok {
		panic(fmt.Errorf("RADBOUDMAPS_DB_BOLT_PASSWORD environment variable not set"))
	}

	dbBolt = fmt.Sprintf("bolt://%s:%s@%s", dbBoltUser, dbBoltPass, dbBoltUrl)
}

func Query(query string, params map[string]interface{}) ([][]interface{}, map[string]interface{}, error) {
	conn, err := driver.OpenNeo(dbBolt)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	stmt, err := conn.PrepareNeo(query)
	if err != nil {
		panic(err)
	}

	defer stmt.Close()

	rows, err := stmt.QueryNeo(params)
	if err != nil {
		panic(err)
	}

	return rows.All()
}
