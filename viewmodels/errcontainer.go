package viewmodels

type ErrorContainer struct {
	Data []Error `json:"errors"`
}
