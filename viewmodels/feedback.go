package viewmodels

type Feedback struct {
	UUID        string `json:"uuid"`
	Reason      string `json:"reason"`
	Explanation string `json:"explanation"`
	From        string `json:"from"`
	FromType    string `json:"from_type"`
	To          string `json:"to"`
	ToType      string `json:"to_type"`
	AppVersion  string `json:"app_version"`
	UserUUID    string `json:"user_uuid"`
	CreatedAt   int64  `json:"created_at"`
}
