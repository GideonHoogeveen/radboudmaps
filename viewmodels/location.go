package viewmodels

type Location struct {
	UUID     string `json:"uuid"`
	Name     string `json:"name"`
	Code     string `json:"code"`
	Building string `json:"building"`
	Floor    int64  `json:"floor"`
}
