package viewmodels

type Person struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}
