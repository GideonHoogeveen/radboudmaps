package viewmodels

type Room struct {
	UUID     string `json:"uuid"`
	Code     string `json:"code"`
	Building string `json:"building"`
	Floor    int64  `json:"floor"`
}
